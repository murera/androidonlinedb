package com.example.androiddasar5onlinedb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<MovieModel> dataMovie = new ArrayList<>();
    RecyclerView recycler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recycler = (RecyclerView) findViewById(R.id.recycleView);

        MovieModel movie1 = new MovieModel();
        movie1.setJudulFilm("Judul");
        movie1.setPosterFilm("https://www.themoviedb.org/t/p/w600_and_h900_bestv2/tnAuB8q5vv7Ax9UAEje5Xi4BXik.jpg");

        for(int i=0; i<10; i++)
            dataMovie.add(movie1);

        recycler.setAdapter(new MovieAdapter(MainActivity.this, dataMovie));

        recycler.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
    }
}